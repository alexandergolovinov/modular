package ee.tech.accountservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AccountService {

    private final RabbitTemplate rabbitTemplate;
    private final AccountRepository accountRepository;
    private final BalanceService balanceService;
    private final ObjectMapper objectMapper;

    @Value("${amqp.queue.name}")
    private String queueName;

    @Autowired
    public AccountService(RabbitTemplate rabbitTemplate, AccountRepository accountRepository, BalanceService balanceService, ObjectMapper objectMapper) {
        this.rabbitTemplate = rabbitTemplate;
        this.accountRepository = accountRepository;
        this.balanceService = balanceService;
        this.objectMapper = objectMapper;
    }

    public Account saveAccount(Account account) {
        Account saveAcc = accountRepository.save(account);
        try {
            log.info("Sending Message");
            String jsonAccount = objectMapper.writeValueAsString(saveAcc);
            rabbitTemplate.convertAndSend(queueName, jsonAccount);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        balanceService.saveAllBalances(saveAcc.getBalanceList());
        log.info("Creating Account with ID: {} for User: {}", saveAcc.getId(), saveAcc.getFullName());
        return saveAcc;
    }

    public Account findAccountById(String id) {
        return accountRepository.findById(Long.valueOf(id)).orElse(null);
    }

    public List<Account> findAllAccounts() {
        List<Account> accounts = new ArrayList<>();
        accountRepository.findAll().forEach(accounts::add);
        return accounts;
    }

}
