package ee.tech.accountservice.service;

import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Balance;
import ee.tech.accountservice.repository.BalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BalanceService {

    private BalanceRepository balanceRepository;

    @Autowired
    public BalanceService(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public List<Balance> saveAllBalances(List<Balance> balanceList) {
        List<Balance> balances = new ArrayList<>();
        balanceRepository.saveAll(balanceList).forEach(balances::add);
        return balances;
    }

    public List<Balance> findAllBalanceByAccount(Account account) {
        return balanceRepository.findAllByAccount(account);
    }

    public Balance saveOrUpdateBalance(Balance balance) {
        return balanceRepository.save(balance);
    }

}
