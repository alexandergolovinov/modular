package ee.tech.accountservice.service;

import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Balance;
import ee.tech.accountservice.entity.Transaction;
import ee.tech.accountservice.exceptions.BalanceNotFoundException;
import ee.tech.accountservice.exceptions.InsufficientFundsException;
import ee.tech.accountservice.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TransactionService {

    private TransactionRepository transactionRepository;
    private BalanceService balanceService;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository, BalanceService balanceService) {
        this.transactionRepository = transactionRepository;
        this.balanceService = balanceService;
    }

    public Transaction calculateTransactionToAccount(Transaction transaction) {
        Balance balance = findBalanceForCurrency(transaction);
        //Do calculation Increment / Decrement
        calculateBalance(balance, transaction);

        //update balance
        balanceService.saveOrUpdateBalance(balance);
        //save transaction
        return transactionRepository.save(transaction);
    }

    public List<Transaction> findTransactionByAccount(Account account) {
        return transactionRepository.findAllByAccount(account);
    }


    /**
     * Helper method to find Balance for Currency
     *
     * @param transaction used to get currency
     * @return Balance
     */
    private Balance findBalanceForCurrency(Transaction transaction) {
        return transaction.getAccount().getBalanceList()
                .stream()
                .filter(b -> b.getCurrency().equals(transaction.getCurrency()))
                .findFirst().orElseThrow(() -> new BalanceNotFoundException("No Balance Found for Currency" + transaction.getCurrency()));
    }

    /**
     * Helper method to calculate balance
     *
     * @param balance     to compute
     * @param transaction used to get amount
     * @return Balance
     */
    private Balance calculateBalance(Balance balance, Transaction transaction) {
        //Money IN
        if (transaction.isDeposit()) {
            balance.setBalance(balance.getBalance() + transaction.getAmount());
            log.info("Deposit balance for account " + transaction.getAccount().getFullName());
            return balance;
        }

        //Money OUT
        if (!transaction.isDeposit()) {
            double value = balance.getBalance() - transaction.getAmount();
            //check if enough funds available
            if (value < 0) {
                log.error("Not enough funds to withdraw for account"
                        + transaction.getAccount().getFullName());
                throw new InsufficientFundsException("Not enough funds to withdraw");
            }
            log.info("Withdraw balance for account " + transaction.getAccount().getFullName());
            balance.setBalance(value);
            return balance;
        }
        return balance;
    }

}
