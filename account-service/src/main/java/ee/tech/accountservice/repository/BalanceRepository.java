package ee.tech.accountservice.repository;

import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Balance;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BalanceRepository extends CrudRepository<Balance, Long> {

    List<Balance> findAllByAccount(Account account);

}
