package ee.tech.accountservice.repository;

import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
    List<Transaction> findAllByAccount(Account account);
}
