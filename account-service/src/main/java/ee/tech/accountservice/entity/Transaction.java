package ee.tech.accountservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.tech.accountservice.entity.enums.Currency;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Account account;
    @Column
    private Currency currency;
    @Column
    private double amount;
    @Column
    private String description;
    @Column
    private boolean isDeposit; //true - deposit | false - withdraw

    public Transaction() {}
}
