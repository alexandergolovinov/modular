package ee.tech.accountservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.tech.accountservice.entity.enums.Currency;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Balance {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Currency currency;
    @Column
    private double balance;
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    private Account account;

    public Balance() {}

    public Balance(Currency currency, double balance, Account account) {
        this.currency = currency;
        this.balance = balance;
        this.account = account;
    }
}
