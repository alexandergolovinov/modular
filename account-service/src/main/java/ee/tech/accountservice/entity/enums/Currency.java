package ee.tech.accountservice.entity.enums;

public enum Currency {
    EUR, SEK, GBP, USD
}
