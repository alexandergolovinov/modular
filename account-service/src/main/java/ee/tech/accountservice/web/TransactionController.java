package ee.tech.accountservice.web;

import ee.tech.accountservice.dto.TransactionDTO;
import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Transaction;
import ee.tech.accountservice.exceptions.AccountNotFoundException;
import ee.tech.accountservice.facade.TransactionFacade;
import ee.tech.accountservice.service.AccountService;
import ee.tech.accountservice.validation.ResponseErrorValidation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "/api/transactions")
@CrossOrigin
public class TransactionController {

    private ResponseErrorValidation responseErrorValidation;
    private AccountService accountService;
    private TransactionFacade transactionFacade;

    @Autowired
    public TransactionController(ResponseErrorValidation responseErrorValidation
            , AccountService accountService, TransactionFacade transactionFacade) {
        this.responseErrorValidation = responseErrorValidation;
        this.accountService = accountService;
        this.transactionFacade = transactionFacade;
    }

    @PostMapping(value = "/create/{accountId}")
    public ResponseEntity<?> createTransaction(@Valid @RequestBody TransactionDTO transactionDTO,
                                               BindingResult bindingResult, @PathVariable String accountId) {
        ResponseEntity<?> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;
        validateAccountExistence(accountId);

        Transaction transaction = transactionFacade.populateToModel(transactionDTO);

        return new ResponseEntity<>(transaction, HttpStatus.CREATED);
    }

    @GetMapping(value = "/all/{accountId}")
    public ResponseEntity<?> findAllTransactionsForAccount(@PathVariable String accountId) {
        validateAccountExistence(accountId);

        List<TransactionDTO> transactionForAccount = transactionFacade.populateTransactionsDTOListByAccount(accountId);
        return new ResponseEntity<>(transactionForAccount, HttpStatus.OK);
    }

    private ResponseEntity<?> validateAccountExistence(String accountId) {
        Account account = accountService.findAccountById(accountId);
        if (ObjectUtils.isEmpty(account)) {
            return new ResponseEntity<>(Map.entry(AccountNotFoundException.class.getSimpleName(),
                    "Account Not Found with ID: " + accountId), HttpStatus.BAD_REQUEST);
        }
        return null;
    }
}
