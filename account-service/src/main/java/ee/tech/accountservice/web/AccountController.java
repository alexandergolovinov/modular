package ee.tech.accountservice.web;

import ee.tech.accountservice.dto.AccountDTO;
import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.exceptions.AccountNotFoundException;
import ee.tech.accountservice.facade.AccountFacade;
import ee.tech.accountservice.service.AccountService;
import ee.tech.accountservice.validation.ResponseErrorValidation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController()
@RequestMapping(value = "/api/accounts")
@CrossOrigin
public class AccountController {

    private AccountService accountService;
    private AccountFacade accountFacade;
    private ResponseErrorValidation responseErrorValidation;

    @Autowired
    public AccountController(AccountService accountService, ResponseErrorValidation responseErrorValidation, AccountFacade accountFacade) {
        this.accountService = accountService;
        this.responseErrorValidation = responseErrorValidation;
        this.accountFacade = accountFacade;
    }


    /**
     * Create bank account for the customer and returns an account object together with balance
     * objects
     */
    @PostMapping(value = "/create")
    public ResponseEntity<?> createAccount(@Valid @RequestBody AccountDTO accountDTO, BindingResult bindingResult) {
        ResponseEntity<?> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) return errors;

        Account account = accountFacade.populateToModel(accountDTO);
        return new ResponseEntity<>(account, HttpStatus.CREATED);
    }

    /**
     * Return the account object by ID
     */
    @GetMapping(value = "/{accountId}")
    public ResponseEntity<?> findAccountById(@PathVariable String accountId) {
        Account account = accountService.findAccountById(accountId);
        if (ObjectUtils.isEmpty(account)) {
            return new ResponseEntity<>(Map.entry(AccountNotFoundException.class.getSimpleName(),
                    "Account Not Found with ID: " + accountId), HttpStatus.BAD_REQUEST);
        }
        AccountDTO accountDTO = accountFacade.populateToDTO(account);
        return new ResponseEntity<>(accountDTO, HttpStatus.OK);
    }

    /**
     * Return all accounts
     */
    @GetMapping(value = "/all")
    public ResponseEntity<?> findAllAccount() {
        List<Account> accounts = accountService.findAllAccounts();
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }


}
