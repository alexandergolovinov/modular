package ee.tech.accountservice.dto;

import ee.tech.accountservice.annotations.ValidCurrency;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class TransactionDTO {

    @NotEmpty(message = "Account ID is required")
    private String accountId;
    @NotEmpty(message = "Description missing")
    private String description;
    @Min(value = 0, message = "Invalid amount")
    private double amount;
    @NotEmpty(message = "Invalid currency")
    @ValidCurrency
    private String currency;
    private double balance;
    private boolean isDeposit;
}
