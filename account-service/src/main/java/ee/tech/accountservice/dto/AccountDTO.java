package ee.tech.accountservice.dto;

import ee.tech.accountservice.annotations.ValidCurrency;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
public class AccountDTO {

    private Long id;
    @NotEmpty(message = "Customer ID is required")
    private String customerID;
    @NotEmpty(message = "Country is required")
    private String country;
    @NotEmpty(message = "At least one Currency is required.")
    @ValidCurrency
    private String currencies;
    @NotEmpty(message = "Account Firstname is required")
    private String firstname;
    @NotEmpty(message = "Account Lastname is required")
    private String lastname;
    private List<BalanceDTO> balanceDTOList = new ArrayList<>();
    private List<TransactionDTO> transactionDTOList = new ArrayList<>();

}
