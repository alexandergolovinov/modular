package ee.tech.accountservice.dto;

import ee.tech.accountservice.entity.enums.Currency;
import lombok.Data;

@Data
public class BalanceDTO {

    private Currency currency;
    private double balance;

    public BalanceDTO(Currency currency, double balance) {
        this.currency = currency;
        this.balance = balance;
    }
}
