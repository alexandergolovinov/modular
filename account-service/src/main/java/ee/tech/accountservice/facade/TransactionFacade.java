package ee.tech.accountservice.facade;

import ee.tech.accountservice.dto.TransactionDTO;
import ee.tech.accountservice.entity.Transaction;
import ee.tech.accountservice.entity.enums.Currency;
import ee.tech.accountservice.service.AccountService;
import ee.tech.accountservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Component
public class TransactionFacade {

    private final TransactionService transactionService;
    private final AccountService accountService;

    @Autowired
    public TransactionFacade(TransactionService transactionService, AccountService accountService) {
        this.transactionService = transactionService;
        this.accountService = accountService;
    }

    public Transaction populateToModel(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction();
        transaction.setAccount(accountService.findAccountById(transactionDTO.getAccountId()));
        transaction.setAmount(transactionDTO.getAmount());
        transaction.setCurrency(Currency.valueOf(StringUtils.trimAllWhitespace(transactionDTO.getCurrency().toUpperCase())));
        transaction.setDeposit(transactionDTO.isDeposit());
        transaction.setDescription(transactionDTO.getDescription());
        transactionService.calculateTransactionToAccount(transaction);
        return transaction;
    }

    public TransactionDTO populateToDTO(Transaction transaction) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setCurrency(transaction.getCurrency().toString());
        transactionDTO.setAccountId(transaction.getAccount().getId().toString());
        transactionDTO.setDescription(transaction.getDescription());
        transactionDTO.setAmount(transaction.getAmount());
        transactionDTO.setDeposit(transaction.isDeposit());
        return transactionDTO;
    }

    public List<TransactionDTO> populateTransactionsDTOListByAccount(String accountId) {
        List<Transaction> transactions = transactionService.findTransactionByAccount(accountService.findAccountById(accountId));
        List<TransactionDTO> transactionDTOList = new ArrayList<>();
        transactions.forEach(t -> {
            TransactionDTO transactionDTO = populateToDTO(t);
            transactionDTOList.add(transactionDTO);
        });
        return transactionDTOList;
    }

}
