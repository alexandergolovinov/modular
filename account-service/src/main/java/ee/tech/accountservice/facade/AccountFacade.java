package ee.tech.accountservice.facade;

import ee.tech.accountservice.dto.AccountDTO;
import ee.tech.accountservice.dto.BalanceDTO;
import ee.tech.accountservice.dto.TransactionDTO;
import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Balance;
import ee.tech.accountservice.entity.enums.Currency;
import ee.tech.accountservice.service.AccountService;
import ee.tech.accountservice.service.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AccountFacade {

    private final AccountService accountService;
    private final BalanceService balanceService;
    private final TransactionFacade transactionFacade;

    @Autowired
    public AccountFacade(AccountService accountService, BalanceService balanceService, TransactionFacade transactionFacade) {
        this.accountService = accountService;
        this.balanceService = balanceService;
        this.transactionFacade = transactionFacade;
    }

    public Account populateToModel(AccountDTO accountDTO) {
        Account account = new Account();
        account.setCountry(accountDTO.getCountry().toUpperCase());
        account.setCustomerID(accountDTO.getCustomerID().toUpperCase());
        account.setFirstname(accountDTO.getFirstname().toUpperCase());
        account.setLastname(accountDTO.getLastname().toUpperCase());
        List<Balance> balances = currencyConverter(accountDTO.getCurrencies(), account);
        account.setBalanceList(balances);
        accountService.saveAccount(account);
        balanceService.saveAllBalances(balances);
        return account;
    }

    public AccountDTO populateToDTO(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setCountry(account.getCountry());
        accountDTO.setCustomerID(account.getCustomerID());
        accountDTO.setFirstname(account.getFirstname());
        accountDTO.setLastname(account.getLastname());
        List<BalanceDTO> balanceDTOList = new ArrayList<>();
        List<Balance> balances = balanceService.findAllBalanceByAccount(account);
        for (Balance balance : balances) {
            BalanceDTO balanceDTO = new BalanceDTO(balance.getCurrency(), balance.getBalance());
            balanceDTOList.add(balanceDTO);
        }
        List<TransactionDTO> transactionDTOList = transactionFacade.populateTransactionsDTOListByAccount(account.getId().toString());
        accountDTO.setTransactionDTOList(transactionDTOList);
        accountDTO.setBalanceDTOList(balanceDTOList);
        return accountDTO;
    }

    private List<Balance> currencyConverter(String accountCurrencies, Account account) {
        List<Balance> balanceList = new ArrayList<>();
        Set<String> currencies = new HashSet<>(Arrays.asList(accountCurrencies.toUpperCase().replaceAll("\\s", "").split(",")));

        for (String currency : currencies) {
            Currency cur = Currency.valueOf((currency).toUpperCase());
            Balance balance = new Balance(cur, 0, account);
            balanceList.add(balance);
        }
        return balanceList;
    }


}
