package ee.tech.accountservice.validation;

import ee.tech.accountservice.annotations.ValidCurrency;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class CurrencyValidator implements ConstraintValidator<ValidCurrency, String> {

    @Override
    public void initialize(ValidCurrency constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        List<String> validCurrencies = Arrays.asList("EUR", "USD", "GBP", "SEK");
        String[] currencies = value.toUpperCase().split(",");

        for (String currency : currencies) {
            if (!validCurrencies.contains(StringUtils.trimWhitespace(currency))) {
                return false;
            }
        }
        return true;
    }
}
