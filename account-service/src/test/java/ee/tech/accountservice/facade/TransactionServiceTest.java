package ee.tech.accountservice.facade;

import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Balance;
import ee.tech.accountservice.entity.Transaction;
import ee.tech.accountservice.entity.enums.Currency;
import ee.tech.accountservice.exceptions.InsufficientFundsException;
import ee.tech.accountservice.repository.TransactionRepository;
import ee.tech.accountservice.service.BalanceService;
import ee.tech.accountservice.service.TransactionService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
class TransactionServiceTest {

    private static Balance balance;
    private static Transaction transaction;

    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private BalanceService balanceService;
    @InjectMocks
    private TransactionService transactionService;

    @BeforeAll
    public static void init() {
        Account account = new Account();
        account.setId(1L);
        account.setCountry("Estonia");
        account.setFirstname("Account firstname");
        account.setLastname("Account lastname");
        account.setCustomerID("FL1");
        account.setId(1L);
        account.setCountry("Estonia");
        account.setCreatedAt(LocalDateTime.of(2020, 10, 14, 10, 10, 10));
        balance = new Balance(Currency.EUR, 350, account);
        account.setBalanceList(Collections.singletonList(balance));

        transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(200);
        transaction.setCurrency(Currency.EUR);
        transaction.setDescription("Transaction Test");
    }

    /**
     * Calculate Deposit to Balance
     */
    @Test
    void testCalculateTransactionToAccountDeposit() {
        transaction.setDeposit(true);
        when(balanceService.saveOrUpdateBalance(balance)).thenReturn(balance);
        when(transactionRepository.save(transaction)).thenReturn(transaction);
        Transaction transaction1 = transactionService.calculateTransactionToAccount(transaction);
        assertEquals(550, balance.getBalance());
    }

    /**
     * Calculate Withdraw
     */
    @Test
    void testCalculateTransactionToAccountWithdraw() {
        transaction.setDeposit(false);
        when(balanceService.saveOrUpdateBalance(balance)).thenReturn(balance);
        when(transactionRepository.save(transaction)).thenReturn(transaction);
        Transaction transaction1 = transactionService.calculateTransactionToAccount(transaction);
        assertEquals(150, balance.getBalance());
    }

    @Test
    void testCalculateTransactionToAccountError() {
        transaction.setDeposit(false);
        transaction.setAmount(1000);

        assertThrows(InsufficientFundsException.class, () -> {
            transactionService.calculateTransactionToAccount(transaction);
        });

    }


}
