package ee.tech.accountservice.facade;

import ee.tech.accountservice.dto.AccountDTO;
import ee.tech.accountservice.dto.BalanceDTO;
import ee.tech.accountservice.dto.TransactionDTO;
import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Balance;
import ee.tech.accountservice.entity.Transaction;
import ee.tech.accountservice.entity.enums.Currency;
import ee.tech.accountservice.service.AccountService;
import ee.tech.accountservice.service.BalanceService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class AccountFacadeTest {

    private static AccountDTO accountDTO;
    private static Account account;
    private static List<Balance> balances;
    private static List<TransactionDTO> transactionDTOS;

    @Mock
    private AccountService accountService;
    @Mock
    private BalanceService balanceService;
    @Mock
    private TransactionFacade transactionFacade;
    @InjectMocks
    private AccountFacade accountFacade;

    @BeforeAll
    public static void init() {
        balances = new ArrayList<>();
        Balance balance = new Balance(Currency.EUR, 400, account);
        balances.add(balance);
        List<BalanceDTO> balanceDTOS = new ArrayList<>();
        List<Transaction> transactions = new ArrayList<>();
        transactionDTOS = new ArrayList<>();

        //Account DTO
        accountDTO = new AccountDTO();
        accountDTO.setCurrencies(Currency.EUR.toString());
        accountDTO.setLastname("AccountDTO lastname");
        accountDTO.setFirstname("AccountDTO firstname");
        accountDTO.setCountry("Estonia_DTO");
        accountDTO.setCustomerID("FL1_DTO");
        accountDTO.setTransactionDTOList(transactionDTOS);
        accountDTO.setBalanceDTOList(balanceDTOS);

        //Account
        account = new Account();
        account.setFirstname("Account firstname");
        account.setLastname("Account lastname");
        account.setCustomerID("FL1");
        account.setId(1L);
        account.setCountry("Estonia");
        account.setBalanceList(balances);
        account.setTransactionList(transactions);
        account.setCreatedAt(LocalDateTime.of(2020, 10, 14, 10, 10, 10));
    }

    @Test
    void testPopulateToModel() {
        when(accountService.saveAccount(account)).thenReturn(account);
        when(balanceService.saveAllBalances(Collections.singletonList(new Balance()))).thenReturn(Collections.singletonList(new Balance()));
        Account account1 = accountFacade.populateToModel(accountDTO);
        assertEquals("FL1_DTO", account1.getCustomerID());
        assertEquals("ACCOUNTDTO LASTNAME", account1.getLastname());
        assertEquals("ESTONIA_DTO", account1.getCountry());
    }

    @Test
    void populateToDTO() {
        when(balanceService.findAllBalanceByAccount(account)).thenReturn(balances);
        when(transactionFacade.populateTransactionsDTOListByAccount(any())).thenReturn(transactionDTOS);
        AccountDTO accountDTO = accountFacade.populateToDTO(account);
        assertEquals(1L, accountDTO.getId());
        assertEquals("FL1", accountDTO.getCustomerID());
    }


}
