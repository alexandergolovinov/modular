package ee.tech.accountservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tech.accountservice.entity.Account;
import ee.tech.accountservice.entity.Balance;
import ee.tech.accountservice.entity.enums.Currency;
import ee.tech.accountservice.repository.AccountRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AccountServiceTest {

    private static Account account;
    private static List<Balance> balances;

    @Mock
    private RabbitTemplate rabbitTemplate;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private BalanceService balanceService;
    @Mock
    private ObjectMapper objectMapper;
    @InjectMocks
    private AccountService accountService;

    @BeforeAll
    public static void init() {
        balances = new ArrayList<>();
        Balance balance = new Balance(Currency.EUR, 400, account);
        balances.add(balance);

        //Account
        account = new Account();
        account.setFirstname("Account firstname");
        account.setLastname("Account lastname");
        account.setCustomerID("FL1");
        account.setId(1L);
        account.setCountry("Estonia");
        account.setBalanceList(balances);
        account.setCreatedAt(LocalDateTime.of(2020, 10, 14, 10, 10, 10));
    }

    @Test
    void testSaveAccount() {
        when(accountRepository.save(account)).thenReturn(account);
        when(balanceService.saveAllBalances(account.getBalanceList())).thenReturn(balances);
        Account account1 = accountService.saveAccount(account);
        assertEquals(400, account1.getBalanceList().stream().findFirst().get().getBalance());
        assertEquals(Currency.EUR, account1.getBalanceList().stream().findFirst().get().getCurrency());
        assertEquals(1L, account1.getId());
    }

    @Test
    void testFindAccountById() {
        when(accountRepository.findById(any())).thenReturn(Optional.of(account));
        Account account1 = accountService.findAccountById("1");
        assertEquals(1L, account1.getId());
        assertEquals("Account firstname", account1.getFirstname());
        assertEquals("Estonia", account1.getCountry());
    }

    @Test
    void testFindAllAccounts() {
        when(accountRepository.findAll()).thenReturn(Collections.singletonList(account));
        List<Account> accounts = accountService.findAllAccounts();
        assertEquals(1L, accounts.stream().findFirst().get().getId());
        assertEquals("Estonia", accounts.stream().findFirst().get().getCountry());

    }

}
