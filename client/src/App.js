import React from 'react';
import './App.css';
import AccountListComponent from "./components/account/AccountListComponent";
import Navigation from "./components/layout/Navigation";
import {
    BrowserRouter as Router,
    Route
} from "react-router-dom";
import AccountFormComponent from "./components/account/AccountFormComponent";
import AccountComponent from "./components/account/AccountComponent";

function App() {
    return (
        <div className="App">
            <Router path="/">
                <Navigation/>
                <Route path="/" exact component={() => <AccountListComponent/>}/>
                <Route path="/create-account" component={() => <AccountFormComponent/>}/>
                <Route path="/accounts/:accountId" render={(props) => <AccountComponent {...props} />}/>
                {/*<Footer/>*/}
            </Router>

        </div>
    );
}

export default App;
