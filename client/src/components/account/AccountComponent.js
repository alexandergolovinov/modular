import React, {Component} from 'react';
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import Table from "react-bootstrap/Table";
import TransactionListComponent from "../transaction/TransactionListComponent";
import TransactionFormComponent from "../transaction/TransactionFormComponent";

class AccountComponent extends Component {
    constructor() {
        super();
        this.state = {
            accountId: "",
            firstname: "",
            lastname: "",
            customerID: "",
            country: "",
            currencies: "",
            balances: [],
            transactions: []
        }
    }

    componentDidMount() {
        const accountId = this.props.match.params.accountId;
        const GET_ACCOUNT = 'http://localhost:8080/api/accounts/' + accountId;

        fetch(GET_ACCOUNT)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    accountId: data.id,
                    firstname: data.firstname,
                    lastname: data.lastname,
                    customerID: data.customerID,
                    country: data.country,
                    currencies: data.currencies,
                    balances: data.balanceDTOList,
                    transactions: data.transactionDTOList
                })
            })
            .catch(err => console.log(err));
    }

    render() {
        const accountId = this.state.accountId;
        return (
            <div className="m-4">
                <div className="row ">
                    <div className="col-6 p-2">
                        <h4 className="display-5 text-center mb-4">Account Details</h4>
                        <Card style={{width: '40rem', float: 'none', margin: '0 auto'}}>
                            <Card.Header><b>Account ID:</b> {accountId}</Card.Header>
                            <ListGroup variant="flush">
                                <ListGroup.Item><b>NAME: </b> {this.state.firstname}</ListGroup.Item>
                                <ListGroup.Item><b>LASTNAME: </b> {this.state.lastname}</ListGroup.Item>
                                <ListGroup.Item><b>COUNTRY: </b>{this.state.country}</ListGroup.Item>
                                <ListGroup.Item><b>CUSTOMER ID: </b> {this.state.customerID}</ListGroup.Item>
                            </ListGroup>
                        </Card>
                        <h4 className="display-5 text-center mt-5 m-4">Available Balances</h4>
                        <Table striped bordered hover size="sm"
                               style={{width: '40rem', float: 'none', margin: '0 auto'}}>
                            <thead>
                            <tr>
                                <th>Currency</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.balances.map((bal, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{bal.currency}</td>
                                        <td>{bal.balance == null ? '0.0' : bal.balance}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </Table>
                    </div>
                    <div className="col-6 p-2">
                        <h4 className="display-5 text-center mb-4">Create Transaction</h4>
                        <TransactionFormComponent accountId={this.props.match.params.accountId}/>

                        <h4 className="display-5 text-center m-4">Transaction History</h4>
                        <TransactionListComponent accountId={this.props.match.params.accountId}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default AccountComponent