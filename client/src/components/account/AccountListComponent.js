import React, {Component} from 'react';
import Table from "react-bootstrap/Table";
import {Link} from "react-router-dom";

class AccountListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accounts: []
        }
    }

    componentDidMount() {
        const GET_ALL_ACCOUNTS = 'http://localhost:8080/api/accounts/all';

        fetch(GET_ALL_ACCOUNTS)
            .then(response => response.json())
            .then(data => this.setState({
                accounts: data
            }))
            .catch(err => console.log(err));
    }

    render() {
        return (
            <div className="align-content-center container-flex m-4">
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Customer ID</th>
                        <th>Country</th>
                        <th>Currencies</th>
                        <th>Transactions</th>
                        <th>View Account</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.accounts.map((account, key) => {
                        return (
                            <tr key={key}>
                                <td>{account.firstname}</td>
                                <td>{account.lastname}</td>
                                <td>{account.customerID}</td>
                                <td>{account.country}</td>
                                <td>{account.balanceList.map(b => b.currency + " ")}</td>
                                <td>{account.transactionList.length == null ? '0' : account.transactionList.length}</td>
                                <td><Link to={'/accounts/' + account.id}>Details</Link></td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default AccountListComponent;