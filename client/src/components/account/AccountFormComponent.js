import React, {Component} from 'react';
import classnames from 'classnames';
import Select from 'react-select'
import { withRouter } from 'react-router';

class AccountFormComponent extends Component {
    constructor() {
        super();
        this.state = {
            accountId: "",
            firstname: "",
            lastname: "",
            customerID: "",
            country: "",
            selectedCurrencies: [],
            currencies: "",
            error: ""
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    onSelectChange = selectedOptions => {
        this.setState({
            selectedCurrencies: selectedOptions
        })
    }

    onSubmit(event) {
        event.preventDefault();
        const POST_NEW_ACCOUNT = 'http://localhost:8080/api/accounts/create';

        let currencies = "";
        this.state.selectedCurrencies.forEach(currency => {
            currencies += currency.value + ",";
        })

        const newAccount = {
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            customerID: this.state.customerID,
            country: this.state.country,
            currencies: currencies
        };

        console.log(newAccount);

        fetch(POST_NEW_ACCOUNT, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(newAccount)
        }).then(response => {
            if (response.ok) {
                this.props.history.push("/");
                return response.json();
            } else {
                throw new Error('Something went wrong');
            }
        }).catch(error => {
            console.error('There was an error!', error);
            this.setState({error: 'There was an error. Please check value.'});
        });

    }

    render() {
        const options = [
            {value: 'EUR', label: 'EUR'},
            {value: 'SEK', label: 'SEK'},
            {value: 'GBP', label: 'GBP'},
            {value: 'USD', label: 'USD'},
        ]
        return (
            <div>
                <div className="container align-content-center mt-3">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h4 className="display-4 text-center">Create an Account</h4>

                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": this.state.error //this will be added if errors object contains errors.
                                    })}
                                           placeholder="Firstname"
                                           name="firstname"
                                           value={this.state.firstname}
                                           onChange={this.onChange}/>
                                    {this.state.error && (
                                        <div className="invalid-feedback">{this.state.error}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": this.state.error //this will be added if errors object contains errors.
                                    })}
                                           placeholder="Lastname"
                                           name="lastname"
                                           value={this.state.lastname}
                                           onChange={this.onChange}/>
                                    {this.state.error && (
                                        <div className="invalid-feedback">{this.state.error}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": this.state.error //this will be added if errors object contains errors.
                                    })}
                                           placeholder="Country"
                                           name="country"
                                           value={this.state.country}
                                           onChange={this.onChange}/>
                                    {this.state.error && (
                                        <div className="invalid-feedback">{this.state.error}</div>
                                    )}
                                </div>
                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": this.state.error //this will be added if errors object contains errors.
                                    })}
                                           placeholder="Unique Customer ID"
                                           name="customerID"
                                           value={this.state.customerID}
                                           onChange={this.onChange}/>
                                    {this.state.error && (
                                        <div className="invalid-feedback">{this.state.error}</div>
                                    )}
                                </div>

                                <div>
                                    <Select
                                        isMulti
                                        placeholder="Select Currencies"
                                        name="currencies"
                                        value={this.state.selectedCurrencies}
                                        options={options}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        onChange={this.onSelectChange}
                                    />
                                </div>

                                <input type="submit" className="btn btn-dark btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(AccountFormComponent);