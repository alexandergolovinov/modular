import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Navigation extends Component {
    render() {
        return (
            <nav className="la-nav navbar navbar-expand bg-dark navbar-dark higher">
                <div className="">
                    <div className="navbar-nav ml-auto">
                        <Link className="nav-item nav-link" to="/create-account">
                            Create Account
                        </Link>
                        <Link className="nav-item nav-link" to="/">
                            All Accounts
                        </Link>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Navigation;