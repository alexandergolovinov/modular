import React, {Component} from 'react';
import Table from "react-bootstrap/Table";

class TransactionListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            transactions: []
        }
    }

    componentDidMount() {
        const accountId = this.props.accountId;
        const GET_ALL_TRANSACTIONS_FOR_ACCOUNT = 'http://localhost:8080/api/transactions/all/' + accountId;
        fetch(GET_ALL_TRANSACTIONS_FOR_ACCOUNT)
            .then(response => response.json())
            .then(data => this.setState({
                transactions: data
            }))
            .catch(err => console.log(err));
    }

    render() {
        return (
            <div>
                <Table striped bordered hover size="sm" variant="dark"
                       style={{width: '40rem', float: 'none', margin: '0 auto'}}>
                    <thead>
                    <tr>
                        <th>Currency</th>
                        <th>Amount</th>
                        <th>Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.transactions.map((transaction, key) => {
                        return (
                            <tr key={key}>
                                <td>{transaction.currency}</td>
                                <td>{transaction.amount}</td>
                                <td className="text-right pr-4">{transaction.deposit === true ? 'DEPOSIT ↑' : 'WITHDRAW ↓'}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>
        )
    }


}

export default TransactionListComponent;