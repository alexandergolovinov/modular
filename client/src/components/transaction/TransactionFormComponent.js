import React, {Component} from 'react';
import classnames from "classnames";
import { withRouter } from 'react-router';

class TransactionFormComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accountId: this.props.accountId,
            currency: "",
            description: "",
            amount: 0,
            deposit: true,
            error: ""
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    onSubmit(event) {
        event.preventDefault();
        const POST_NEW_TRANSACTION = 'http://localhost:8080/api/transactions/create/' + this.state.accountId;

        const newTransaction = {
            accountId: this.state.accountId,
            currency: this.state.currency,
            description: this.state.description,
            amount: this.state.amount,
            deposit: this.state.deposit
        };

        console.log(newTransaction);
        fetch(POST_NEW_TRANSACTION, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(newTransaction)
        }).then(response => {
            if (response.ok) {
                window.location.reload();
                return response.json();
            } else {
                throw new Error('Something went wrong');
            }
        }).catch(error => {
            console.error('There was an error!', error);
            this.setState({error: 'There was an error. Please check value.'});
        });
    }

    render() {
        return (
            <div>
                <div className="container align-content-center mt-3">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input type="number" className={classnames("form-control form-control-lg", {
                                        "is-invalid": this.state.error //this will be added if errors object contains errors.
                                    })}
                                           placeholder="Amount"
                                           name="amount"
                                           value={this.state.amount}
                                           onChange={this.onChange}/>
                                    {this.state.error && (
                                        <div className="invalid-feedback">{this.state.error}</div>
                                    )}
                                </div>

                                <div className="form-group">
                                    <select className="form-control form-control-lg"
                                            name="currency"
                                            value={this.state.currency}
                                            defaultChecked="EUR"
                                            onChange={this.onChange}>
                                        <option value="EUR">EUR</option>
                                        <option value="GBP">GBP</option>
                                        <option value="USD">USD</option>
                                        <option value="SEK">SEK</option>
                                    </select>
                                </div>

                                <div className="form-group">
                                    <select className="form-control form-control-lg"
                                            name="deposit"
                                            value={this.state.deposit}
                                            onChange={this.onChange}>
                                        <option value="true">DEPOSIT</option>
                                        <option value="false">WITHDRAW</option>
                                    </select>
                                </div>

                                <div className="form-group">
                                    <input type="text" className={classnames("form-control form-control-lg", {
                                        "is-invalid": this.state.error //this will be added if errors object contains errors.
                                    })}
                                           placeholder="Short description"
                                           name="description"
                                           value={this.state.description}
                                           onChange={this.onChange}/>
                                    {this.state.error && (
                                        <div className="invalid-feedback">{this.state.error}</div>
                                    )}
                                </div>

                                <input type="submit" className="btn btn-dark btn-block mt-4"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default withRouter(TransactionFormComponent);