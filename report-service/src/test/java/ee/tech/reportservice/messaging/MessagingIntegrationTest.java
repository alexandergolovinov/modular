package ee.tech.reportservice.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tech.reportservice.entity.Account;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@SpringBootTest
class MessagingIntegrationTest {

    private static Account account;

    @Mock
    private ObjectMapper objectMapper;

    @BeforeAll
    public static void init() {
        account = new Account();
        account.setId(1L);
        account.setCustomerID("C1");
        account.setFirstname("Name1");
        account.setLastname("Lastname1");
    }

    @Test
    void testReceiveMessage() throws JsonProcessingException {
        ReportServiceProcessor reportServiceProcessor = mock(ReportServiceProcessor.class);
        doNothing().when(reportServiceProcessor).receiveMessage(isA(String.class));
        reportServiceProcessor.receiveMessage(objectMapper.writeValueAsString(account));
        verify(reportServiceProcessor, times(1)).receiveMessage(objectMapper.writeValueAsString(account));
    }
}
