package ee.tech.reportservice.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, updatable = false)
    private String customerID;
    @Column
    private String country;
    @Column
    private String firstname;
    @Column
    private String lastname;

    public Account() {
    }

}
