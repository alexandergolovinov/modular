package ee.tech.reportservice.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.tech.reportservice.entity.Account;
import ee.tech.reportservice.repository.AccountRepository;
import ee.tech.reportservice.services.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class ReportServiceProcessor {

    private final ObjectMapper objectMapper;
    private final AccountService accountService;

    @Autowired
    public ReportServiceProcessor(ObjectMapper objectMapper, AccountService accountService) {
        this.objectMapper = objectMapper;
        this.accountService = accountService;
    }

    public void receiveMessage(String accountJson) {
        //you can do with received message here
        log.info("Message received");

        try {
            Account account = objectMapper.readValue(accountJson, Account.class);
            log.info("Account received with ID: {}", account.getId());
            accountService.saveAccount(account);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
