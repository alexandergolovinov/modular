package ee.tech.reportservice.repository;

import ee.tech.reportservice.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
}
